package test.wizi4d.games.framework.math;

import com.wizi4d.games.framework.math.Circle;
import com.wizi4d.games.framework.math.Point2D;

import junit.framework.TestCase;

public class CircleTest extends TestCase {

	public void testCircle() {
		float posX = 15;
		float posY = 20;
		float radius = 5;
		Circle tester = new Circle(posX, posY, radius);
		assertEquals(radius, tester.radius);
		assertEquals(new Point2D(posX, posY), tester.center);
	}
	
	public void testIsPointInside() {
		float[] posX = new float[] {10, 20, 18, 10};
		float[] posY = new float[] {10, 10, 18, 21};
		boolean[] result = new boolean[] {true, true, false, false};
		int maxIndex = Math.max(posX.length, posY.length);
		maxIndex = Math.max(maxIndex, result.length);
		
		Circle tester = new Circle(10, 10, 10);
		
		// float float
		for (int i = 0; i < maxIndex; ++i) {
			assertEquals(result[i], tester.isPointInside(posX[i], posY[i]));
		}
		
		// Point2D
		Point2D point = new Point2D(10,10);
		for (int i = 0; i < maxIndex; ++i) {
			point.set(posX[i], posY[i]);
			assertEquals(result[i], tester.isPointInside(point));
		}
	}
}
