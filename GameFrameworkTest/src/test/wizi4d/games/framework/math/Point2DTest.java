package test.wizi4d.games.framework.math;

import com.wizi4d.games.framework.math.Point2D;

import junit.framework.TestCase;

public class Point2DTest extends TestCase {
	private static final float DEFAULT_X = 50f;
	private static final float DEFAULT_Y = 50f;
	
	public void testConstructors() {
		// no arguments
		Point2D tester = new Point2D();
		testXYValues(0.0f, 0.0f, tester);
		
		// float float
		tester = new Point2D(DEFAULT_X, DEFAULT_Y);
		testXYValues(DEFAULT_X, DEFAULT_Y, tester);
		
		// Point2D
		Point2D defaultPoint2D = createDefaultPoint2D();
		tester = new Point2D(defaultPoint2D);
		assertEquals(defaultPoint2D, tester);
		assertNotSame(defaultPoint2D, tester);
	}
	
	public void testCopy() {
		Point2D tester = createDefaultPoint2D();
		Point2D testerCopy = tester.copy();
		assertEquals(tester, testerCopy);
		assertNotSame(tester, testerCopy);
	}

	public void testSet() {
		// float float
		Point2D tester = createDefaultPoint2D();
		float newX = 5f;
		float newY = 4f;
		Point2D result = tester.set(newX, newY);
		assertSame(tester, result);
		testXYValues(newX, newY, result);
		
		// Point2D
		float anotherPoint2DX = 17f;
		float anotherPoint2DY = 28f;
		tester = createDefaultPoint2D();
		Point2D anotherPoint2D = new Point2D(anotherPoint2DX, anotherPoint2DY);
		result = tester.set(anotherPoint2D);
		assertSame(tester, result);
		assertEquals(anotherPoint2D, result);
		assertNotSame(anotherPoint2D, result);
	}

	public void testDist() {
		// float float
		Point2D tester = new Point2D(10f, 10f);
		assertEquals(1, tester.dist(11f, 10f), 0.0f);
		assertEquals(2, tester.dist(8f, 10f), 0.0f);
		assertEquals(8, tester.dist(10f, 18f), 0.0f);
		assertEquals(15, tester.dist(10f, -5f), 0.0f);
		assertEquals(9.22, tester.dist(19f, 12f), 0.01f);
		assertEquals(15.30, tester.dist(-5f, 13f), 0.01f);
		
		// Point2D
		tester = new Point2D(10f, 10f);
		assertEquals(1, tester.dist(new Point2D(11f, 10f)), 0.0f);
		assertEquals(2, tester.dist(new Point2D(8f, 10f)), 0.0f);
		assertEquals(8, tester.dist(new Point2D(10f, 18f)), 0.0f);
		assertEquals(15, tester.dist(new Point2D(10f, -5f)), 0.0f);
		assertEquals(9.22, tester.dist(new Point2D(19f, 12f)), 0.01f);
		assertEquals(15.30, tester.dist(new Point2D(-5f, 13f)), 0.01f);
	}

	public void testDistSquared() {
		// float float
		Point2D tester = new Point2D(10f, 10f);
		assertEquals(1, tester.distSquared(11f, 10f), 0.0f);
		assertEquals(4, tester.distSquared(8f, 10f), 0.0f);
		assertEquals(64, tester.distSquared(10f, 18f), 0.0f);
		assertEquals(225, tester.distSquared(10f, -5f), 0.0f);
		assertEquals(85, tester.distSquared(19f, 12f), 0.0f);
		assertEquals(234, tester.distSquared(-5f, 13f), 0.0f);
		
		// Point2D
		tester = new Point2D(10f, 10f);
		assertEquals(1, tester.distSquared(new Point2D(11f, 10f)), 0.0f);
		assertEquals(4, tester.distSquared(new Point2D(8f, 10f)), 0.0f);
		assertEquals(64, tester.distSquared(new Point2D(10f, 18f)), 0.0f);
		assertEquals(225, tester.distSquared(new Point2D(10f, -5f)), 0.0f);
		assertEquals(85, tester.distSquared(new Point2D(19f, 12f)), 0.0f);
		assertEquals(234, tester.distSquared(new Point2D(-5f, 13f)), 0.0f);
	}

	public void testAdd() {
		// float float
		float addX = 43f;
		float addY = 23f;
		Point2D tester = createDefaultPoint2D();
		Point2D result = tester.add(addX, addY);
		assertSame(tester, result);
		testXYValues(DEFAULT_X + addX, DEFAULT_Y + addY, result);
		
		// Point2D
		float anotherPoint2DX = 46f;
		float anotherPoint2DY = 11f;
		Point2D anotherPoint2D = new Point2D(anotherPoint2DX, anotherPoint2DY);
		tester = createDefaultPoint2D();
		result = tester.add(anotherPoint2D);
		assertSame(tester, result);
		testXYValues(DEFAULT_X + anotherPoint2D.x, DEFAULT_Y + anotherPoint2D.y, result);
	}

	public void testSub() {
		// float float
		float subX = 3f;
		float subY = 1f;
		Point2D tester = createDefaultPoint2D();
		Point2D result = tester.sub(subX, subY);
		assertSame(tester, result);
		testXYValues(DEFAULT_X - subX, DEFAULT_Y - subY, result);
		
		// Point2D
		float anotherPoint2DX = 1f;
		float anotherPoint2DY = 5f;
		Point2D anotherPoint2D = new Point2D(anotherPoint2DX, anotherPoint2DY);
		tester = createDefaultPoint2D();
		result = tester.sub(anotherPoint2D);
		assertSame(tester, result);
		testXYValues(DEFAULT_X - anotherPoint2D.x, DEFAULT_Y - anotherPoint2D.y, result);
	}

	public void testMul() {
		float scalar = 3;
		Point2D tester = createDefaultPoint2D();
		Point2D result = tester.mul(scalar);
		assertSame(tester, result);
		testXYValues(DEFAULT_X * scalar, DEFAULT_Y * scalar, result);
	}
	
	private Point2D createDefaultPoint2D() {
		return new Point2D(DEFAULT_X, DEFAULT_Y);
	}
	
	private void testXYValues(float expectedX, float expectedY, Point2D actual) {
		assertEquals(expectedX, actual.x, 0.0f);
		assertEquals(expectedY, actual.y, 0.0f);
	}
}
