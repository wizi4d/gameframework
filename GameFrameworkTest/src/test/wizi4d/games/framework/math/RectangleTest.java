package test.wizi4d.games.framework.math;

import com.wizi4d.games.framework.math.Point2D;
import com.wizi4d.games.framework.math.Rectangle;

import junit.framework.TestCase;

public class RectangleTest extends TestCase {

	public void testRectangle() {
		float lowerX = 10;
		float lowerY = 10;
		float width = 5;
		float height = 10;
		Rectangle tester = new Rectangle(lowerX, lowerY, width, height);
		assertEquals(new Point2D(lowerX, lowerY), tester.lowerLeft);
		assertEquals(width, tester.width);
		assertEquals(height, tester.height);
	}
	
	public void testIsPointInside() {
		float[] posX = new float[] {10, 15, 3, 10};
		float[] posY = new float[] {10, 8, 18, 23};
		boolean[] result = new boolean[] {true, true, false, false};
		int maxIndex = Math.max(posX.length, posY.length);
		maxIndex = Math.max(maxIndex, result.length);
		
		Rectangle tester = new Rectangle(5, 5, 10, 15);
		
		// float float
		for (int i = 0; i < maxIndex; ++i) {
			assertEquals(result[i], tester.isPointInside(posX[i], posY[i]));
		}
		
		// Point2D
		Point2D point = new Point2D(10,10);
		for (int i = 0; i < maxIndex; ++i) {
			point.set(posX[i], posY[i]);
			assertEquals(result[i], tester.isPointInside(point));
		}
	}

}
