package test.wizi4d.games.framework.math;

import com.wizi4d.games.framework.math.Vector2D;

import junit.framework.TestCase;

public class Vector2DTest extends TestCase {
	
	public void testCope() {
		Vector2D tester = new Vector2D(17.0f, 3.0f);
		Vector2D testerCopy = tester.copy();
		assertEquals(tester, testerCopy);
		assertNotSame(tester, testerCopy);
	}
	
	public void testLen() {
		Vector2D tester = new Vector2D(2.0f, 0.0f);
		assertEquals(2f, tester.len(), 0.0f);
		
		tester.set(0.0f, 4.0f);
		assertEquals(4.0f, tester.len(), 0.0f);
		
		tester.set(2.0f, 3.0f);
		assertEquals(3.61f, tester.len(), 0.01f);
	}

	public void testNormalize() {
		Vector2D tester = new Vector2D(5.0f, 9.0f);
		Vector2D result = tester.normalize();
		assertSame(tester, result);
		assertEquals(1.0f, result.len(), 0.01f);
	}

	public void testAngle() {
		Vector2D tester = new Vector2D(1.0f, 0.0f);
		assertEquals(0.0f, tester.angle(), 0.0f);
		
		tester.set(0.0f, 1.0f);
		assertEquals(90.0f, tester.angle(), 0.0f);
		
		tester.set(2.0f, -2.0f);
		assertEquals(315.0f, tester.angle(), 0.0f);
	}

	public void testRotate() {
		Vector2D tester = new Vector2D(2.0f, 0.0f);
		Vector2D result = tester.rotate(90.0f);
		assertSame(tester, result);
		assertEquals(0.0f, result.x, 0.01f);
		assertEquals(2.0f, result.y, 0.01f);
		assertEquals(90.0f, result.angle(), 0.01f);
	}
}
