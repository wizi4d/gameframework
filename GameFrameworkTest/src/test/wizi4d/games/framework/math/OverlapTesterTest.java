package test.wizi4d.games.framework.math;

import com.wizi4d.games.framework.math.Circle;
import com.wizi4d.games.framework.math.OverlapTester;
import com.wizi4d.games.framework.math.Point2D;
import com.wizi4d.games.framework.math.Rectangle;

import junit.framework.TestCase;

public class OverlapTesterTest extends TestCase {

	public void testIsPointInRectangle() {
		float[] posX = new float[] {10, 15, 3, 10};
		float[] posY = new float[] {10, 8, 18, 23};
		boolean[] result = new boolean[] {true, true, false, false};
		int maxIndex = Math.max(posX.length, posY.length);
		maxIndex = Math.max(maxIndex, result.length);
		
		Rectangle tester = new Rectangle(5, 5, 10, 15);
		
		// float float
		for (int i = 0; i < maxIndex; ++i) {
			assertEquals(result[i], OverlapTester.isPointInRectangle(tester, posX[i], posY[i]));
		}
		
		// Point2D
		Point2D point = new Point2D(10,10);
		for (int i = 0; i < maxIndex; ++i) {
			point.set(posX[i], posY[i]);
			assertEquals(result[i], OverlapTester.isPointInRectangle(tester, point));
		}
	}

	public void testIsPointInCircle() {
		float[] posX = new float[] {10, 20, 18, 10};
		float[] posY = new float[] {10, 10, 18, 21};
		boolean[] result = new boolean[] {true, true, false, false};
		int maxIndex = Math.max(posX.length, posY.length);
		maxIndex = Math.max(maxIndex, result.length);
		
		Circle tester = new Circle(10, 10, 10);
		
		// float float
		for (int i = 0; i < maxIndex; ++i) {
			assertEquals(result[i], OverlapTester.isPointInCircle(tester, posX[i], posY[i]));
		}
		
		// Point2D
		Point2D point = new Point2D(10,10);
		for (int i = 0; i < maxIndex; ++i) {
			point.set(posX[i], posY[i]);
			assertEquals(result[i], OverlapTester.isPointInCircle(tester, point));
		}
	}

	public void testIsShapesOverlapCircleCircle() {
		Circle circle1 = new Circle(10, 10, 5);
		Circle circle2 = new Circle(15, 15, 3);
		Circle circle3 = new Circle(0, 0, 10);
		
		assertTrue(OverlapTester.isShapesOverlap(circle1, circle2));
		assertTrue(OverlapTester.isShapesOverlap(circle1, circle3));
		assertFalse(OverlapTester.isShapesOverlap(circle2, circle3));
	}

	public void testIsShapesOverlapCircleRectangle() {
		Circle circle1 = new Circle(6, 5, 15);
		Circle circle2 = new Circle(25, 25, 8);
		Rectangle rectangle1 = new Rectangle(5, 5, 10, 10);
		Rectangle rectangle2 = new Rectangle(20, 30, 10, 15);
		
		assertTrue(OverlapTester.isShapesOverlap(circle1, rectangle1));
		assertTrue(OverlapTester.isShapesOverlap(circle2, rectangle2));
		assertFalse(OverlapTester.isShapesOverlap(circle1, rectangle2));
		assertFalse(OverlapTester.isShapesOverlap(circle2, rectangle1));
	}

	public void testIsShapesOverlapRectangleRectangle() {
		Rectangle rectangle1 = new Rectangle(13, 12, 11, 7);
		Rectangle rectangle2 = new Rectangle(22, 14, 3, 8);
		Rectangle rectangle3 = new Rectangle(5, 7, 8.1f, 6);
		
		assertTrue(OverlapTester.isShapesOverlap(rectangle1, rectangle2));
		assertTrue(OverlapTester.isShapesOverlap(rectangle1, rectangle3));
		assertFalse(OverlapTester.isShapesOverlap(rectangle2, rectangle3));
	}

}
