package test.wizi4d.games.framework;

import com.wizi4d.games.framework.GameObject;
import com.wizi4d.games.framework.math.Point2D;
import com.wizi4d.games.framework.math.Rectangle;

import junit.framework.TestCase;

public class GameObjectTest extends TestCase {
	private final static float DEFAULT_POS_X = 5.0f;
	private final static float DEFAULT_POS_Y = 8.0f;
	private final static float DEFAULT_WIDTH = 20.0f;
	private final static float DEFAULT_HEIGHT = 10.0f;
	
	private GameObject tester;
	
	protected void setUp() throws Exception {
		super.setUp();
		tester = new GameObject(DEFAULT_POS_X, DEFAULT_POS_Y, DEFAULT_WIDTH, DEFAULT_HEIGHT);
	}
	
	public void testGameObject() {
		assertNotNull(tester);
		assertEquals(new Point2D(DEFAULT_POS_X, DEFAULT_POS_Y), tester.position);
		float lowerLeftX = DEFAULT_POS_X - DEFAULT_WIDTH / 2;
		float lowerLeftY = DEFAULT_POS_Y - DEFAULT_HEIGHT / 2;
		assertEquals(new Rectangle(lowerLeftX, lowerLeftY, DEFAULT_WIDTH, DEFAULT_HEIGHT), tester.bounds);
	}
	
	public void testUpdateBounds() {
		float newPosX = 7.0f;
		float newPosY = 9.0f;
		tester.position.set(newPosX, newPosY);
		tester.updateBounds();
		float lowerLeftX = newPosX - DEFAULT_WIDTH / 2;
		float lowerLeftY = newPosY - DEFAULT_HEIGHT / 2;
		assertEquals(new Rectangle(lowerLeftX, lowerLeftY, DEFAULT_WIDTH, DEFAULT_HEIGHT), tester.bounds);
	}

}
