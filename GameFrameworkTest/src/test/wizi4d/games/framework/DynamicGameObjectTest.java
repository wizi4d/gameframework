package test.wizi4d.games.framework;

import com.wizi4d.games.framework.DynamicGameObject;
import com.wizi4d.games.framework.math.Point2D;
import com.wizi4d.games.framework.math.Vector2D;

import junit.framework.TestCase;

public class DynamicGameObjectTest extends TestCase {
	
	private DynamicGameObject tester;
	
	protected void setUp() throws Exception {
		super.setUp();
		tester = new DynamicGameObject(5.0f, 5.0f, 2.0f, 2.0f);
	}
	
	public void testDynamicGameObject() {
		assertNotNull(tester);
	}
	
	public void testUpdatePossition() {
		Point2D startPossition = tester.position.copy();
		tester.velocity.set(2.0f, 1.0f);
		tester.updatePossition(1.0f);
		assertEquals(startPossition.x + tester.velocity.x, tester.position.x);
		assertEquals(startPossition.y + tester.velocity.y, tester.position.y);
	}
	
	public void testUpdateVelocity() {
		tester.velocity.set(1.0f, 0.0f);
		tester.acceleration.set(-1.0f, 0.0f);
		Vector2D startVelocity = tester.velocity.copy();
		tester.updateVelocity(1.0f);
		assertEquals(startVelocity.x + tester.acceleration.x, tester.velocity.x);
		assertEquals(startVelocity.y + tester.acceleration.y, tester.velocity.y);
	}
}
