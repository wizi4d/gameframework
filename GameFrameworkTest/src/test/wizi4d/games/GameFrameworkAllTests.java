package test.wizi4d.games;

import junit.framework.Test;
import junit.framework.TestSuite;
import android.test.suitebuilder.TestSuiteBuilder;

public class GameFrameworkAllTests extends TestSuite {
    public static Test suite() {
        return new TestSuiteBuilder(GameFrameworkAllTests.class).includeAllPackagesUnderHere().build();
    }
}