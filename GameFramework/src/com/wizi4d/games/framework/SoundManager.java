package com.wizi4d.games.framework;

public interface SoundManager {
	public Sound newSound(String filename);
}
