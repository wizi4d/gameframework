package com.wizi4d.games.framework;

import com.wizi4d.games.framework.math.Vector2D;

public class DynamicGameObject extends GameObject {
	public final Vector2D velocity;
	public final Vector2D acceleration;
	
	public DynamicGameObject(float x, float y, float width, float height) {
		super(x, y, width, height);
		velocity = new Vector2D();
		acceleration = new Vector2D();
	}
	
	public void updatePossition(float deltaTime) {
		position.add(velocity);
	}
	
	public void updateVelocity(float deltaTime) {
		velocity.add(acceleration);
	}
}
