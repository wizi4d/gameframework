package com.wizi4d.games.framework.android;

import java.util.List;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

public class AccelerometerHandler implements SensorEventListener {
	private SensorManager manager;
	private Sensor accelerometer;
	private boolean accelerometerExists;
	private float accelerationX;
	private float accelerationY;
	private float accelerationZ;
	
	public AccelerometerHandler(Context context) {
		manager = (SensorManager) context.getSystemService(Context.SEARCH_SERVICE);
		List<Sensor> sensorsList = manager.getSensorList(Sensor.TYPE_ACCELEROMETER);
		accelerometerExists = (sensorsList.size() != 0);
		if (accelerometerExists) {
			accelerometer = sensorsList.get(0);
		}
	}
	
	public boolean start() {
		return manager.registerListener(this, accelerometer, SensorManager.SENSOR_DELAY_GAME);
	}
	
	public void stop() {
		manager.unregisterListener(this, accelerometer);
	}
	
	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) {
		// ��� ���� ��� ����� ����������?
	}

	@Override
	public void onSensorChanged(SensorEvent event) {
		accelerationX = event.values[0];
		accelerationY = event.values[1];
		accelerationZ = event.values[2];
	}
	
	public boolean isAccelerometerExists() {
		return accelerometerExists;
	}
	
	public float getAccelerationX() {
		return accelerationX;
	}
	
	public float getAccelerationY() {
		return accelerationY;
	}
	
	public float getAccelerationZ() {
		return accelerationZ;
	}
}
