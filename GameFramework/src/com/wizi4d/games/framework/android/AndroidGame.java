package com.wizi4d.games.framework.android;

import com.wizi4d.games.framework.Screen;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.view.Window;
import android.view.WindowManager;

public abstract class AndroidGame extends Activity {
	protected Screen currentScreen;
	private WakeLock wakeLock;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		
		PowerManager powerManager = (PowerManager) getSystemService(Context.POWER_SERVICE);
		wakeLock = powerManager.newWakeLock(PowerManager.FULL_WAKE_LOCK, "AndroidGame");
	}
	
	@Override
	public void onResume() {
		super.onResume();
		wakeLock.acquire();
    }
	
	@Override 
	public void onPause() {
		wakeLock.release();
		super.onPause();
    }
	
	public abstract Screen getStartScreen();
	
	public void setScreen(Screen newScreen) {
		if (newScreen == null)
			throw new IllegalArgumentException("Screen must not be null");
		
		currentScreen.pause();
		currentScreen.dispose();
		
		currentScreen = newScreen;
		currentScreen.resume();
		//currentScreen.update(0);
	}
}
