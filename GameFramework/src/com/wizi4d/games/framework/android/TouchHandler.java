package com.wizi4d.games.framework.android;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import com.wizi4d.games.framework.Pool;
import com.wizi4d.games.framework.Pool.PoolObjectFactory;

import android.view.View;
import android.view.View.OnTouchListener;

public abstract class TouchHandler implements OnTouchListener {
	public static class TouchEvent {
		public static final int TOUCH_DOWN = 0;
		public static final int TOUCH_UP = 1;
		public static final int TOUCH_MOVE = 2;
		
		public int type;
		public int x;
		public int y;
		public int pointer;
	}
	
	public static final class TouchEventFactory implements PoolObjectFactory<TouchEvent> {
		@Override
		public TouchEvent createObject() {
			return new TouchEvent();
		}
	}
	
	private static final int MAX_EVENTS = 100;
	
	protected Pool<TouchEvent> touchEventPool;
	protected List<TouchEvent> touchEvents = new ArrayList<TouchEvent>();
	//protected List<TouchEvent> touchEventsBuffer = new ArrayList<TouchEvent>();
	protected Queue<TouchEvent> touchEventsBuffer = new LinkedList<TouchEvent>();
	
	public TouchHandler(View owner) {
		touchEventPool = new Pool<TouchHandler.TouchEvent>(new TouchEventFactory(), MAX_EVENTS);
		owner.setOnTouchListener(this);
	}
	
	public void addTouchEvent(TouchEvent touchEvent) {
		if (touchEventsBuffer.size() == MAX_EVENTS) {
			TouchEvent touchEventToFree = touchEventsBuffer.poll();
			touchEventPool.free(touchEventToFree);
		}
		touchEventsBuffer.offer(touchEvent);
		//touchEventsBuffer.add(touchEvent);
	}
	
	public List<TouchEvent> getTouchEvents() {
		synchronized(this) {
			int len = touchEvents.size();
			for (int i = 0; i < len; ++i)
				touchEventPool.free(touchEvents.get(i));
			touchEvents.clear();
			touchEvents.addAll(touchEventsBuffer);
			touchEventsBuffer.clear();
			
			return touchEvents;
		}
	}
}
