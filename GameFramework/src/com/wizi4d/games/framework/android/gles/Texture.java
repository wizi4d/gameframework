package com.wizi4d.games.framework.android.gles;

import java.io.IOException;
import java.io.InputStream;

import javax.microedition.khronos.opengles.GL10;

import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.opengl.GLUtils;

public class Texture {
	private String filename;
	private GLGraphics glGraphics;
	private AssetManager assetManager;
	private int textureId;
	private int width;
	private int height;
	private int minifyingFilter;
	private int magnificationFilter;
	
	public Texture(GLGraphics glGraphics, AssetManager assetManager, String filename) {
		this.glGraphics = glGraphics;
		this.assetManager = assetManager;
		this.filename = filename;
		
		load();
		setFilters(GL10.GL_NEAREST, GL10.GL_NEAREST);
		unbind();
	}
	
	private Bitmap tryLoadAsset(String filename) {
		Bitmap bitmap = null;
		InputStream stream = null;
		try {
			stream = assetManager.open(filename);
//			BitmapFactory.Options options = new BitmapFactory.Options();
//			options.inPreferredConfig = Bitmap.Config.ARGB_8888;
//			Bitmap bitmap = BitmapFactory.decodeStream(stream, null, options);
			bitmap = BitmapFactory.decodeStream(stream);
		} catch(IOException e) {
			throw new RuntimeException("couldn't load asset '" + filename + "'");
		} finally {
			if (stream != null) {
				try {
					stream.close();
				} catch (IOException e) {
				}
			}
		}
		
		return bitmap;
	}
	
	private void load() {
		GL10 gl = glGraphics.getGL();
		int textureIds[] = new int[1];
		gl.glGenTextures(1, textureIds, 0);
		textureId = textureIds[0];
		
		Bitmap bitmap = tryLoadAsset(filename);
		if (bitmap != null) {
			width = bitmap.getWidth();
			height = bitmap.getHeight();
			
			bind();
			GLUtils.texImage2D(GL10.GL_TEXTURE_2D, 0, bitmap, 0);
			
			bitmap.recycle();
		}
	}
	
	public void reload() {
		load();
		setFilters(minifyingFilter, magnificationFilter);
		unbind();
	}
	
	public void bind() {
		GL10 gl = glGraphics.getGL();
		gl.glBindTexture(GL10.GL_TEXTURE_2D, textureId);
	}
	
	public void unbind() {
		GL10 gl = glGraphics.getGL();
		gl.glBindTexture(GL10.GL_TEXTURE_2D, 0);
	}
	
	public void setFilters(int minifyingFilter, int magnificationFilter) {
		GL10 gl = glGraphics.getGL();
		gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MIN_FILTER, minifyingFilter);
		gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MAG_FILTER, magnificationFilter);
		this.minifyingFilter = minifyingFilter;
		this.magnificationFilter = magnificationFilter;
	}
	
	public void dispose() {
		GL10 gl = glGraphics.getGL();
		int[] textureIDs = { textureId };
		gl.glDeleteTextures(1, textureIDs, 0);
	}
	
	public int getWidth() {
		return width;
	}
	
	public int getHeight() {
		return height;
	}
}
