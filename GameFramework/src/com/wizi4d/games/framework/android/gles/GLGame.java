package com.wizi4d.games.framework.android.gles;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import com.wizi4d.games.framework.android.AndroidGame;

import android.opengl.GLSurfaceView;
import android.opengl.GLSurfaceView.Renderer;
import android.os.Bundle;

public abstract class GLGame extends AndroidGame implements Renderer {
	enum OpenGLGameStates {
		Initialized,
		Running,
		Paused,
		Finished,
		Idle
	}
	
	private long startTime;
	private OpenGLGameStates state = OpenGLGameStates.Initialized;
	private Object stateLocker = new Object();
	private GLGraphics glGraphics;
	private GLSurfaceView glView;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		glView = new GLSurfaceView(this);
		//glView.setEGLConfigChooser(8, 8, 8, 8, 0, 0);
		glView.setRenderer(this);
		setContentView(glView);
		
		glGraphics = new GLGraphics();
	}
	
	@Override
	public void onResume() {
		super.onResume();
		glView.onResume();
    }
	
	@Override 
	public void onPause() {
		synchronized(stateLocker) {
			if (isFinishing())
				state = OpenGLGameStates.Finished;
			else
				state = OpenGLGameStates.Paused;
			
			while (true) {
				try {
					stateLocker.wait();
					break;
				} catch (InterruptedException e) {
				}
			}
		}
		
		glView.onPause();
		super.onPause();
    } 
	
	@Override
	public void onSurfaceCreated(GL10 gl, EGLConfig config) {
		glGraphics.setGL(gl);
		synchronized (stateLocker) {
			if (state == OpenGLGameStates.Initialized)
				currentScreen = getStartScreen();
			state = OpenGLGameStates.Running;
			currentScreen.resume();
			startTime = System.nanoTime();
		}
	}
	
	@Override
	public void onDrawFrame(GL10 gl) {
		OpenGLGameStates currentState = null;
		
		synchronized(stateLocker) {
			currentState = state;
		}
		
		if (currentState == OpenGLGameStates.Running) {
			long previousTime = startTime;
			startTime = System.nanoTime();
			float deltaTime = (startTime - previousTime) / 1000000000.0f;
			currentScreen.update(deltaTime);
			currentScreen.present(deltaTime);
		}
		
		if (currentState == OpenGLGameStates.Paused) {
			currentScreen.pause();
			synchronized(stateLocker) {
				state = OpenGLGameStates.Idle;
				stateLocker.notifyAll();
			}
		}
		
		if (currentState == OpenGLGameStates.Finished) {
			currentScreen.pause();
			currentScreen.dispose();
			synchronized(stateLocker) {
				state = OpenGLGameStates.Idle;
				stateLocker.notifyAll();
			}
		}
	}

	@Override
	public void onSurfaceChanged(GL10 gl, int width, int height) {
	}
	
	public GLGraphics getGLGraphics() {
		return glGraphics;
	}
	
	public GLSurfaceView getGLView() {
		return glView;
	}
}