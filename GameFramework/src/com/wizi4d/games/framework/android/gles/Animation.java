package com.wizi4d.games.framework.android.gles;

public class Animation {
	public static final int ANIMATION_LOOPING = 0;
	public static final int ANIMATION_NONLOOPING = 0;
	
	private final RectTextureRegion[] frames;
	private final float frameDuraion;
	
	public Animation(float frameDuraion, RectTextureRegion ... frames) {
		this.frameDuraion = frameDuraion;
		this.frames = frames;
	}
	
	public RectTextureRegion getFrame(float startTime, int mode) {
		int frameNumber = (int)(startTime / frameDuraion);
		if (mode == ANIMATION_NONLOOPING)
			frameNumber = Math.min(frames.length - 1, frameNumber);
		else
			frameNumber = frameNumber % frames.length;
		
		return frames[frameNumber];
	}
}
