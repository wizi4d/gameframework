package com.wizi4d.games.framework.android.gles;

public class Vertices2D extends Vertices {
	public Vertices2D(int maxVertices, int maxIndices, boolean hasColor, boolean hasTextureCoordinates) {
		super(2, maxVertices, maxIndices, hasColor, hasTextureCoordinates);
	}
	
	public Vertices2D(int maxVertices, boolean hasTextureCoordinates) {
		super(2, maxVertices, hasTextureCoordinates);
	}
	
	public Vertices2D(int maxVertices, int maxIndices, boolean hasTextureCoordinates) {
		super(2, maxVertices, maxIndices, hasTextureCoordinates);
	}
}
