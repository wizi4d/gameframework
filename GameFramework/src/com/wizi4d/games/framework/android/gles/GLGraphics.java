package com.wizi4d.games.framework.android.gles;

import javax.microedition.khronos.opengles.GL10;


public class GLGraphics {
	private GL10 gl;
	
	void setGL(GL10 gl) {
		this.gl = gl;
	}
	
	public GL10 getGL() {
		return gl;
	}
}
