package com.wizi4d.games.framework.android.gles;

import javax.microedition.khronos.opengles.GL10;

//import com.wizi4d.games.framework.GameObject;

public abstract class ViewModel {
	protected final Vertices vertices;
	protected final Texture texture;
//	GameObject gameObject;
	
	public ViewModel(int dementionsCount, int maxVertices, Texture texture) {
		this.vertices = new Vertices(dementionsCount, maxVertices, true);
		this.texture = texture;
	}
	
	public ViewModel(int dementionsCount, int maxVertices, int maxIndices, Texture texture) {
		this.vertices = new Vertices(dementionsCount, maxVertices, maxIndices, true);
		this.texture = texture;
	}
	
	public ViewModel(int dementionsCount, int maxVertices, int maxIndices, boolean hasColor) {
		this.vertices = new Vertices(dementionsCount, maxVertices, maxIndices, hasColor, false);
		this.texture = null;
	}
	
	public ViewModel(int dementionsCount, int maxVertices, int maxIndices, boolean hasColor, Texture texture) {
		this.vertices = new Vertices(dementionsCount, maxVertices, maxIndices, hasColor, true);
		this.texture = texture;
	}
	
	public void draw(GL10 gl) {
		//vertices.draw(gl, GL10.GL_TRIANGLES, 0, (vertices.indices == null ? vertices.vertices.capacity() : vertices.indices.capacity()));
	}
	
	public void reload() {
		texture.reload();
	}
	
//	public ViewModel(GameObject gameObject) {
//		this.gameObject = gameObject;
//	}
	
//	public void setGameObject(GameObject gameObject) {
//		this.gameObject = gameObject;
//	}
}
