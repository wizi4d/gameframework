package com.wizi4d.games.framework.android.gles;

import javax.microedition.khronos.opengles.GL10;

public abstract class Square {
	protected static final int VERTOCES_COUNT = 4;
	protected static final int INDICES_COUNT = 6;
	
	protected final Vertices vertices;
	
	public Square(boolean hasColor, boolean hasTexture) {
		vertices = new Vertices2D(VERTOCES_COUNT, INDICES_COUNT, hasColor, hasTexture);
	}
	
	public abstract void render(GL10 gl);
}
