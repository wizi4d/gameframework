package com.wizi4d.games.framework.android.gles;

import com.wizi4d.games.framework.Screen;

public abstract class GLScreen implements Screen {
	protected GLGame glGame;
	
	public GLScreen(GLGame glGame) {
		this.glGame = glGame;
	}
}
