package com.wizi4d.games.framework.android.gles;

public class RectTextureRegion {
	public final float s1, t1;
	public final float s2, t2;
	
	public RectTextureRegion(Texture texture, float x, float y, float width, float height) {
		int textureWidth = texture.getWidth();
		int textureHeight = texture.getHeight();
		s1 = x / textureWidth;
		t1 = y / textureHeight;
		s2 = (x + width) / textureWidth;
		t2 = (y + height) / textureHeight;
	}
	
	public static void fillTextureRegions(RectTextureRegion[] regions, Texture texture,
			int regionsCount, int regionsPerRow,
			int regionWidth, int regionHeight,
			int offsetX, int offsetY) {
		
		int x = offsetX;
		int y = offsetY;
		for (int i = 0; i < regionsCount; ++i) {
			regions[i] = new RectTextureRegion(texture, x, y, regionWidth, regionHeight);
			x += regionWidth;
			if (x == (offsetX + regionWidth * regionsPerRow)) {
				x = offsetX;
				y += regionHeight;
			}
		}
	}
}
