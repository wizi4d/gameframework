package com.wizi4d.games.framework.android.gles;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;

import javax.microedition.khronos.opengles.GL10;

public class Vertices {
	public final int dimentionsCount;
	public final boolean hasColor;
	public final boolean hasTextureCoordinates;
	public final int vertexSize;
	private final FloatBuffer vertices;
	private final ShortBuffer indices;
	
	public Vertices(int dementionsCount, int maxVertices, int maxIndixes, boolean hasColor, boolean hasTextureCoordinates) {
		this.dimentionsCount = dementionsCount;
		this.hasColor = hasColor;
		this.hasTextureCoordinates = hasTextureCoordinates;
		vertexSize = (dementionsCount + (hasColor ? 4 : 0) + (hasTextureCoordinates ? 2 : 0)) * Float.SIZE / 8;
		
		ByteBuffer buffer = ByteBuffer.allocateDirect(maxVertices * vertexSize);
		buffer.order(ByteOrder.nativeOrder());
		vertices = buffer.asFloatBuffer();
		
		if (maxIndixes > 0) {
			buffer = ByteBuffer.allocateDirect(maxIndixes * Short.SIZE / 8);
			buffer.order(ByteOrder.nativeOrder());
			indices = buffer.asShortBuffer();
		}
		else 
			indices = null;
	}
	
	public Vertices(int dementionsCount, int maxVertices, boolean hasTextureCoordinates) {
		this(dementionsCount, maxVertices, 0, false, hasTextureCoordinates);
	}
	
	public Vertices(int dementionsCount, int maxVertices, int maxIndices, boolean hasTextureCoordinates) {
		this(dementionsCount, maxVertices, maxIndices, false, hasTextureCoordinates);
	}
	
	public void setVertices(float[] vertices, int offset, int length) {
		this.vertices.clear();
		this.vertices.put(vertices, offset, length);
		this.vertices.flip();
	}
	
	public void setIndices(short[] indices, int offset, int length) {
		this.indices.clear();
		this.indices.put(indices, offset, length);
		this.indices.flip();
	}
	
	public void draw(GL10 gl, int primitiveType, int offset, int numVertices) {
		enableGLStates(gl);
		
		if (indices != null) {
			indices.position(offset);
			gl.glDrawElements(primitiveType, numVertices, GL10.GL_UNSIGNED_SHORT, indices);
		}
		else
			gl.glDrawArrays(primitiveType, offset, numVertices);
		
		disableGLStates(gl);
	}
	
	private void enableGLStates(GL10 gl) {
		gl.glEnableClientState(GL10.GL_VERTEX_ARRAY);
		vertices.position(0);
		gl.glVertexPointer(dimentionsCount, GL10.GL_FLOAT, vertexSize, vertices);
		
		if (hasColor) {
			gl.glEnableClientState(GL10.GL_COLOR_ARRAY);
			vertices.position(dimentionsCount);
			gl.glColorPointer(4, GL10.GL_FLOAT, vertexSize, vertices);
		}
		
		if (hasTextureCoordinates) {
			gl.glEnableClientState(GL10.GL_TEXTURE_COORD_ARRAY);
			vertices.position(dimentionsCount + (hasColor ? 4 : 0));
			gl.glTexCoordPointer(2, GL10.GL_FLOAT, vertexSize, vertices);
		}
	}
	
	private void disableGLStates(GL10 gl) {
		if (hasTextureCoordinates)
			gl.glDisableClientState(GL10.GL_TEXTURE_COORD_ARRAY);
		
		if (hasColor)
			gl.glDisableClientState(GL10.GL_COLOR_ARRAY);
		
		gl.glDisableClientState(GL10.GL_VERTEX_ARRAY);
	}
}
