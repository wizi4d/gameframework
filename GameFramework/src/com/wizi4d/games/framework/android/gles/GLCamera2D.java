package com.wizi4d.games.framework.android.gles;

import javax.microedition.khronos.opengles.GL10;

import com.wizi4d.games.framework.math.Point2D;

import android.view.View;

public class GLCamera2D {
	public float zoom;
	public final Point2D position;
	
	private final float viewFrustumWidth;
	private final float viewFrustumHeight;
	private View owner;
	private GLGraphics glGraphics;
	
	public GLCamera2D(View owner, GLGraphics glGraphics, float viewFrustumWidth, float viewFrustumHeight) {
		this.owner = owner;
		this.glGraphics = glGraphics;
		
		position = new Point2D(viewFrustumWidth / 2, viewFrustumHeight / 2);
		this.viewFrustumWidth = viewFrustumWidth;
		this.viewFrustumHeight = viewFrustumHeight;
		this.zoom = 1.0f;
	}
	
	public void setViewportAndMatrices() {
		GL10 gl = glGraphics.getGL();
		gl.glViewport(0, 0, owner.getWidth(), owner.getHeight());
		gl.glMatrixMode(GL10.GL_PROJECTION);
		gl.glLoadIdentity();
		gl.glOrthof(position.x - viewFrustumWidth * zoom / 2,
					position.x + viewFrustumWidth * zoom / 2,
					position.y - viewFrustumHeight * zoom / 2,
					position.y + viewFrustumHeight * zoom / 2,
					1, -1);
		gl.glMatrixMode(GL10.GL_MODELVIEW);
		gl.glLoadIdentity();
	}
	
	public void touchToGameWorld(Point2D touch) {
		touch.x = ((touch.x / (float) owner.getWidth()) * viewFrustumWidth * zoom) + position.x - (viewFrustumWidth * zoom / 2);
		touch.y = ((1 - touch.y / (float) owner.getHeight()) * viewFrustumHeight * zoom) + position.y - (viewFrustumHeight * zoom / 2);
	}
}
