package com.wizi4d.games.framework.android.gles;

public abstract class ResourceManager {
	protected GLGame glGame;
	
	protected ResourceManager(GLGame glGame) {
		this.glGame = glGame;
	}
	
	protected abstract void reload();
	
	abstract public void dispose();
}
