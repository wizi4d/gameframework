package com.wizi4d.games.framework.android.gles;

import javax.microedition.khronos.opengles.GL10;

import com.wizi4d.games.framework.math.Point2D;

public class Sprite extends Square {
	private final Texture texture;
	
	public Sprite(Texture texture) {
		super(false, true);
		this.texture = texture;
	}
	
	public void fillVertices(float positionX, float positionY, float width, float height, RectTextureRegion textureRegion) {
		float x1 = positionX - width / 2;
		float y1 = positionY - height / 2;
		float x2 = x1 + width;
		float y2 = y1 + height;
		
		float[] verticesBuffer = new float[VERTOCES_COUNT * vertices.vertexSize / (Float.SIZE / 8)];
		
		int verticesCounter = 0;
		verticesBuffer[verticesCounter++] = x1;
		verticesBuffer[verticesCounter++] = y1;
		verticesBuffer[verticesCounter++] = textureRegion.s1;
		verticesBuffer[verticesCounter++] = textureRegion.t2;
		
		verticesBuffer[verticesCounter++] = x1;
		verticesBuffer[verticesCounter++] = y2;
		verticesBuffer[verticesCounter++] = textureRegion.s1;
		verticesBuffer[verticesCounter++] = textureRegion.t1;
		
		verticesBuffer[verticesCounter++] = x2;
		verticesBuffer[verticesCounter++] = y2;
		verticesBuffer[verticesCounter++] = textureRegion.s2;
		verticesBuffer[verticesCounter++] = textureRegion.t1;
		
		verticesBuffer[verticesCounter++] = x2;
		verticesBuffer[verticesCounter++] = y1;
		verticesBuffer[verticesCounter++] = textureRegion.s2;
		verticesBuffer[verticesCounter++] = textureRegion.t2;
		
		vertices.setVertices(verticesBuffer, 0, verticesBuffer.length);
		
		short[] indicesBuffer = new short[] {0, 1, 2, 0, 2, 3};
		
		vertices.setIndices(indicesBuffer, 0, indicesBuffer.length);
	}
	
	public void fillVertices(Point2D position, float width, float height, RectTextureRegion textureRegion) {
		fillVertices(position.x, position.y, width, height, textureRegion);
	}
	
	public void render(GL10 gl) {
		gl.glEnable(GL10.GL_TEXTURE_2D);
		texture.bind();
		vertices.draw(gl, GL10.GL_TRIANGLES, 0, INDICES_COUNT);
		texture.unbind();
	}
}
