package com.wizi4d.games.framework.android.gles;

import javax.microedition.khronos.opengles.GL10;

import com.wizi4d.games.framework.math.Point2D;

public class SingleColorSquare extends Square {
	private float r, g, b, a;
	private boolean onlyBorder;
	
	public SingleColorSquare() {
		super(true, false);
		onlyBorder = false;
	}
	
	public SingleColorSquare(boolean onlyBorder) {
		super(true, false);
		this.onlyBorder = onlyBorder;
	}
	
	public void setColor(float r, float g, float b, float a) {
		this.r = r;
		this.g = g;
		this.b = b;
		this.a = a;
	}
	
	public void fillVertices(float positionX, float positionY, float width, float height) {
		float x1 = positionX - width / 2;
		float y1 = positionY - height / 2;
		float x2 = x1 + width;
		float y2 = y1 + height;
		
		float[] verticesBuffer = new float[VERTOCES_COUNT * vertices.vertexSize / (Float.SIZE / 8)];
		
		int verticesCounter = 0;
		verticesBuffer[verticesCounter++] = x1;
		verticesBuffer[verticesCounter++] = y1;
		verticesCounter = addColorToBuffer(verticesBuffer, verticesCounter);
		
		verticesBuffer[verticesCounter++] = x1;
		verticesBuffer[verticesCounter++] = y2;
		verticesCounter = addColorToBuffer(verticesBuffer, verticesCounter);
		
		verticesBuffer[verticesCounter++] = x2;
		verticesBuffer[verticesCounter++] = y2;
		verticesCounter = addColorToBuffer(verticesBuffer, verticesCounter);
		
		verticesBuffer[verticesCounter++] = x2;
		verticesBuffer[verticesCounter++] = y1;
		verticesCounter = addColorToBuffer(verticesBuffer, verticesCounter);
		
		vertices.setVertices(verticesBuffer, 0, verticesBuffer.length);
		
		short[] indicesBuffer = null;
		indicesBuffer = new short[] {0, 1, 2, 0, 2, 3};
		/*if (onlyBorder) {
			indicesBuffer = new short[] {0, 1, 2, 3, 1, 2};
		}
		else {
			indicesBuffer = new short[] {0, 1, 2, 0, 2, 3};
		}*/
		
		vertices.setIndices(indicesBuffer, 0, indicesBuffer.length);
	}
	
	private int addColorToBuffer(float[] verticesBuffer, int verticesCounter) {
		verticesBuffer[verticesCounter++] = r;
		verticesBuffer[verticesCounter++] = g;
		verticesBuffer[verticesCounter++] = b;
		verticesBuffer[verticesCounter++] = a;
		
		return verticesCounter;
	}
	
	public void fillVertices(Point2D position, float width, float height) {
		fillVertices(position.x, position.y, width, height);
	}
	
	@Override
	public void render(GL10 gl) {
		if (onlyBorder) {
			vertices.draw(gl, GL10.GL_LINE_LOOP, 0, INDICES_COUNT);
		}
		else {
			vertices.draw(gl, GL10.GL_TRIANGLES, 0, INDICES_COUNT);
		}
	}
}
