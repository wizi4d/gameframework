package com.wizi4d.games.framework.android;

import java.io.IOException;

import android.app.Activity;
import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;
import android.media.SoundPool;

import com.wizi4d.games.framework.SoundManager;
import com.wizi4d.games.framework.Sound;

public class AndroidSoundManager implements SoundManager {
	private AssetManager assets;
	private SoundPool soundPool;
	
	public AndroidSoundManager(Activity owner){
		assets = owner.getAssets();
		soundPool = new SoundPool(20, android.media.AudioManager.STREAM_MUSIC, 0);
	}
	
	@Override
	public Sound newSound(String filename) {
		try {
            AssetFileDescriptor assetDescriptor = assets.openFd(filename);
            int soundId = soundPool.load(assetDescriptor, 0);
            return new AndroidSound(soundPool, soundId);
        } catch (IOException e) {
            throw new RuntimeException("Couldn't load sound '" + filename + "'");
        }
	}
}
