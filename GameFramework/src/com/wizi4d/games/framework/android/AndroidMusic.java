package com.wizi4d.games.framework.android;

import java.io.IOException;

import android.app.Activity;
import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;

import com.wizi4d.games.framework.Music;

public class AndroidMusic implements Music, OnCompletionListener {
	private AssetManager assetManager;
	private MediaPlayer mediaPlayer;
	private boolean isPrepared = false;
	
	public AndroidMusic(Activity owner) {
		this.assetManager = owner.getAssets();
		mediaPlayer = new MediaPlayer();
		mediaPlayer.setOnCompletionListener(this);
	}
	
	public void playNewMusic(String filename, boolean looping, float leftVolume, float rightVolume){
		setDataSource(filename);
		setLooping(looping);
		setVolume(leftVolume, rightVolume);
		play();
	}
	
	@Override
	public void setDataSource(String filename) {
		mediaPlayer.reset();
		try {
            AssetFileDescriptor assetDescriptor = assetManager.openFd(filename);
            synchronized (this) {
            	mediaPlayer.setDataSource(assetDescriptor.getFileDescriptor(), assetDescriptor.getStartOffset(), assetDescriptor.getLength());
                mediaPlayer.prepare();
            	isPrepared = true;
			}
        } catch (IOException e) {
            throw new RuntimeException("Couldn't load music '" + filename + "'");
		} catch (Exception e) {
	        throw new RuntimeException("Couldn't load music");
	    }
	}
	
	@Override
	public void play() {
		if (mediaPlayer.isPlaying())
            return;

        try {
            synchronized (this) {
                if (!isPrepared)
                    mediaPlayer.prepare();
                mediaPlayer.start();
            }
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
	}
	
	@Override
	public void stop() {
		mediaPlayer.stop();
        synchronized (this) {
            isPrepared = false;
        }
	}
	
	@Override
	public void pause() {
		if (mediaPlayer.isPlaying())
			mediaPlayer.pause();
	}

	@Override
	public void setLooping(boolean looping) {
		mediaPlayer.setLooping(looping);
	}
	
	@Override
	public void setVolume(float volume) {
		setVolume(volume, volume);
	}
	
	@Override
	public void setVolume(float leftVolume, float rightVolume) {
		mediaPlayer.setVolume(leftVolume, rightVolume);
	}

	@Override
	public boolean isPlaying() {
		return mediaPlayer.isPlaying();
	}
	
	@Override
	public boolean isStopped() {
		return !isPrepared;
	}
	
	@Override
	public boolean isLooping() {
		return mediaPlayer.isLooping();
	}

	@Override
	public void dispose() {
		if (mediaPlayer.isPlaying())
            mediaPlayer.stop();
        mediaPlayer.release();
	}

	@Override
	public void onCompletion(MediaPlayer mp) {
		synchronized (this) {
            isPrepared = false;
        }
	}
}
