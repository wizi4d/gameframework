package com.wizi4d.games.framework.android;

import android.media.SoundPool;

import com.wizi4d.games.framework.Sound;

public class AndroidSound implements Sound {
	private int soundID;
	private SoundPool soundPool;
    
	public AndroidSound(SoundPool soundPool,int soundID) {
    	this.soundID = soundID;
    	this.soundPool = soundPool;
	}
    
	@Override
	public void play(float volume) {
		soundPool.play(soundID, volume, volume, 0, 0, 1);
	}
	
	@Override
	public void play(float leftVolume, float rightVolume) {
		soundPool.play(soundID, leftVolume, rightVolume, 0, 0, 1);		
	}
	
	@Override
	public void dispose() {
		soundPool.unload(soundID);
	}
}
