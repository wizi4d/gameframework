package com.wizi4d.games.framework.android;

import android.view.MotionEvent;
import android.view.View;

public class SingleTouchHandler extends TouchHandler {
	
	public SingleTouchHandler(View owner) {
		super(owner);
	}
	
	@Override
	public boolean onTouch(View v, MotionEvent event) {
		synchronized (this) {
			TouchEvent touchEvent = touchEventPool.newObject();
			switch (event.getAction()) {
			case MotionEvent.ACTION_DOWN:
				touchEvent.type = TouchEvent.TOUCH_DOWN;
				break;
			case MotionEvent.ACTION_MOVE:
				touchEvent.type = TouchEvent.TOUCH_MOVE;
				break;
			case MotionEvent.ACTION_CANCEL:
			case MotionEvent.ACTION_UP:
				touchEvent.type = TouchEvent.TOUCH_UP;
				break;
			}
			touchEvent.x = (int) event.getX(); 
			touchEvent.y = (int) event.getY();
			
			addTouchEvent(touchEvent);
		}
		
		return true;
	}
}
