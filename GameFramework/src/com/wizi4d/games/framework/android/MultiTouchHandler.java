package com.wizi4d.games.framework.android;

import android.view.MotionEvent;
import android.view.View;

public class MultiTouchHandler extends TouchHandler {
	
	public MultiTouchHandler(View owner) {
		super(owner);
	}
	
	@Override
	public boolean onTouch(View v, MotionEvent event) {
		synchronized (this) {
			int action = event.getAction() & MotionEvent.ACTION_MASK;
			int pointerIndex = (event.getAction() & MotionEvent.ACTION_POINTER_ID_MASK) >> MotionEvent.ACTION_POINTER_ID_SHIFT;
			int pointerId = event.getPointerId(pointerIndex);
			
			TouchEvent touchEvent;
			switch (action) {
			case MotionEvent.ACTION_DOWN:
			case MotionEvent.ACTION_POINTER_DOWN:
				touchEvent = touchEventPool.newObject();
				touchEvent.type = TouchEvent.TOUCH_DOWN;
				touchEvent.pointer = pointerId;
				touchEvent.x = (int) event.getX(pointerIndex);
				touchEvent.y = (int) event.getY(pointerIndex);
				addTouchEvent(touchEvent);
				break;
			
			case MotionEvent.ACTION_UP:
			case MotionEvent.ACTION_POINTER_UP:
			case MotionEvent.ACTION_CANCEL:
				touchEvent = touchEventPool.newObject();
				touchEvent.type = TouchEvent.TOUCH_UP;
				touchEvent.pointer = pointerId;
				touchEvent.x = (int) event.getX(pointerIndex);
				touchEvent.y = (int) event.getY(pointerIndex);
				addTouchEvent(touchEvent);
				break;
			
			case MotionEvent.ACTION_MOVE:
				int pointerCount = event.getPointerCount();
				for (pointerIndex = 0; pointerIndex < pointerCount; ++pointerIndex) {
					pointerId = event.getPointerId(pointerIndex);
					touchEvent = touchEventPool.newObject();
					touchEvent.type = TouchEvent.TOUCH_MOVE;
					touchEvent.pointer = pointerId;
					touchEvent.x = (int) event.getX(pointerIndex);
					touchEvent.y = (int) event.getY(pointerIndex);
					addTouchEvent(touchEvent);
				}
				break;
			}
		}
		
		return true;
	}
}
