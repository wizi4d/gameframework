package com.wizi4d.games.framework.math;

public class Point2D {
	public float x, y;
	
	public Point2D() {
	}
	
	public Point2D(float x, float y) {
		this.x = x;
		this.y = y;
	}
	
	public Point2D(Point2D other) {
		this(other.x, other.y);
	}
	
	@Override
	public boolean equals(Object obj) {
		boolean result = false;
		if (obj != null) {
			Point2D point2D = (Point2D)obj;
			result = (this.x == point2D.x) && (this.y == point2D.y);
		}
		
		return result;
	}
	
	@Override
	public String toString() {
		return super.toString() + String.format("{x = %.2f; y = %.2f}", x, y);
	}
	
	public Point2D copy() {
		return new Point2D(x, y);
	}
	
	public Point2D set(float x, float y) {
		this.x = x;
		this.y = y;
		
		return this;
	}
	
	public Point2D set(Point2D other) {
		return set(other.x, other.y);
	}
	
	public float dist(float x, float y) {
		float distX = this.x - x;
		float distY = this.y - y;
		
		return (float)Math.sqrt(distX*distX + distY*distY);
	}
	
	public float dist(Point2D other) {
		return dist(other.x, other.y);
	}
	
	public float distSquared(float x, float y) {
		float distX = this.x - x;
		float distY = this.y - y;
		
		return distX*distX + distY*distY;
	}
	
	public float distSquared(Point2D other) {
		return distSquared(other.x, other.y);
	}
	
	public Point2D add(float x, float y) {
		this.x += x;
		this.y += y;
		
		return this;
	}
	
	public Point2D add(Point2D other) {
		return add(other.x, other.y);
	}
	
	public Point2D sub(float x, float y) {
		this.x -= x;
		this.y -= y;
		
		return this;
	}
	
	public Point2D sub(Point2D other) {
		return sub(other.x, other.y);
	}
	
	public Point2D mul(float scalar) {
		this.x *= scalar;
		this.y *= scalar;
		
		return this;
	}
}