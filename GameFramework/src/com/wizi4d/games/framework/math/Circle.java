package com.wizi4d.games.framework.math;

public class Circle extends Shape {
	public final Point2D center;
	public float radius;
	
	public Circle(float x, float y, float radius) {
		center = new Point2D(x, y);
		this.radius = radius;
	}

	@Override
	public boolean isPointInside(float x, float y) {
		return OverlapTester.isPointInCircle(this, x, y);
	}

	@Override
	public boolean isPointInside(Point2D point) {
		return OverlapTester.isPointInCircle(this, point);
	}
}
