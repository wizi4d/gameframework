package com.wizi4d.games.framework.math;

public class OverlapTester {
	
	public static boolean isPointInRectangle(Rectangle rectangle, float x, float y) {
		return (rectangle.lowerLeft.x <= x)
				&& (rectangle.lowerLeft.x + rectangle.width >= x)
				&& (rectangle.lowerLeft.y <= y)
				&& (rectangle.lowerLeft.y + rectangle.height >= y);
	}
	
	public static boolean isPointInRectangle(Rectangle rectangle, Point2D point) {
		return isPointInRectangle(rectangle, point.x, point.y);
	}
	
	public static boolean isPointInCircle(Circle circle, float x, float y) {
		return (circle.center.distSquared(x, y) <= circle.radius * circle.radius);
	}
	
	public static boolean isPointInCircle(Circle circle, Point2D point) {
		return isPointInCircle(circle, point.x, point.y);
	}
	
	public static boolean isShapesOverlap(Circle circle1, Circle circle2) {
		float distanceSquared = circle1.center.distSquared(circle2.center);
		float radiusSum = circle1.radius + circle2.radius;
		
		return (distanceSquared <= radiusSum * radiusSum);
	}

	public static boolean isShapesOverlap(Circle circle, Rectangle rectangle) {
		float closestX = circle.center.x;
		float closestY = circle.center.y;
		
		if (closestX < rectangle.lowerLeft.x) {
			closestX = rectangle.lowerLeft.x;
		}
		else if (closestX > rectangle.lowerLeft.x + rectangle.width) {
			closestX = rectangle.lowerLeft.x + rectangle.width;
		}
		
		if (closestY < rectangle.lowerLeft.y) {
			closestY = rectangle.lowerLeft.y;
		}
		else if (closestY > rectangle.lowerLeft.y + rectangle.height) {
			closestY = rectangle.lowerLeft.y + rectangle.height;
		}
		
		return (circle.center.distSquared(closestX, closestY) <= circle.radius * circle.radius);
	}
	
	public static boolean isShapesOverlap(Rectangle rectangle1, Rectangle rectangle2) {
		return (rectangle1.lowerLeft.x < rectangle2.lowerLeft.x + rectangle2.width
				&& rectangle1.lowerLeft.x + rectangle1.width > rectangle2.lowerLeft.x
				&& rectangle1.lowerLeft.y < rectangle2.lowerLeft.y + rectangle2.height
				&& rectangle1.lowerLeft.y + rectangle1.height > rectangle2.lowerLeft.y);
	}
}
