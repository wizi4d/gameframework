package com.wizi4d.games.framework.math;

public class Rectangle extends Shape {
	public final Point2D lowerLeft;
	public float width;
	public float height;
	
	public Rectangle(float x, float y, float width, float height) {
		lowerLeft = new Point2D(x, y);
		this.width = width;
		this.height = height;
	}
	
	@Override
	public boolean equals(Object object) {
		Rectangle rectangle = (Rectangle)object;
		
		return this.lowerLeft.equals(rectangle.lowerLeft) && this.width == rectangle.width && this.height == rectangle.height;
	}

	@Override
	public boolean isPointInside(float x, float y) {
		return OverlapTester.isPointInRectangle(this, x, y);
	}

	@Override
	public boolean isPointInside(Point2D point) {
		return OverlapTester.isPointInRectangle(this, point);
	}
	
}
