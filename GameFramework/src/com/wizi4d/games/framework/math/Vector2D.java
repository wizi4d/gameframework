package com.wizi4d.games.framework.math;

public class Vector2D extends Point2D {
	public static float TO_RADIANS = (1 / 180.0f) * (float)Math.PI;
	public static float TO_DEGREES = (1 / (float)Math.PI) * 180;
	
	public Vector2D() {
		super();
	}
	
	public Vector2D(float x, float y) {
		super(x, y);
	}
	
	public Vector2D(Point2D other) {
		super(other);
	}
	
	@Override
	public Vector2D copy() {
		return new Vector2D(x, y);
	}
	
	public float len() {
		return (float)Math.sqrt(x*x + y*y);
	}
	
	public Vector2D normalize() {
		float len = len();
		if(len!=0) {
			this.x /= len;
			this.y /= len;
		}
		
		return this;
	}
	
	public float angle() {
		float angle = (float)Math.atan2(y, x) * TO_DEGREES;
		if(angle < 0)
			angle += 360;
		
		return angle;
	}
	
	public Vector2D rotate(float angle) {
		float rad = angle * TO_RADIANS;
		float cos = (float) Math.cos(rad);
		float sin = (float) Math.sin(rad);
		
		float newX = this.x * cos - this.y * sin;
		float newY = this.x * sin + this.y * cos;
		
		this.x = newX;
		this.y = newY;
		
		return this;
	}
}
