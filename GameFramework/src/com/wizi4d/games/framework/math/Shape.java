package com.wizi4d.games.framework.math;

public abstract class Shape {
	public abstract boolean isPointInside(float x, float y);
	public abstract boolean isPointInside(Point2D point);
}
