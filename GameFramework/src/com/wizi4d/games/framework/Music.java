package com.wizi4d.games.framework;

public interface Music {
	public void playNewMusic(String filename, boolean looping, float leftVolume, float rightVolume);
	
	public void setDataSource(String filename);
	
	public void play();
	public void stop();
	public void pause();
	
	public void setLooping(boolean looping);
	public void setVolume(float volume);
	public void setVolume(float leftVolume, float rightVolume);
	
	public boolean isPlaying();
	public boolean isStopped();
	public boolean isLooping();
	
	public void dispose();
}
