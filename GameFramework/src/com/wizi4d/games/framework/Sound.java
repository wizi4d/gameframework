package com.wizi4d.games.framework;

public interface Sound {
	public void play(float volume);
	public void play(float leftVolume, float rightVolume);
	public void dispose();
}
