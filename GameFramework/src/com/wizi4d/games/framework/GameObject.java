package com.wizi4d.games.framework;

import com.wizi4d.games.framework.math.Point2D;
import com.wizi4d.games.framework.math.Rectangle;

public class GameObject {
	public final Point2D position;
	public final Rectangle bounds;
	
	public GameObject(float x, float y, float width, float height) {
		position = new Point2D(x, y);
		bounds = new Rectangle(x - width / 2, y - height / 2, width, height);
	}
	
	public void updateBounds() {
		bounds.lowerLeft.set(position).sub(bounds.width / 2, bounds.height / 2);
	}
}
